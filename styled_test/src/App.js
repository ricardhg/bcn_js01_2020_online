import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import Componente from './Compo';
import Marcador from './Marcador';

import Display from './DisplayContador';
import Boton from './BtnContador';

import { LIMITE_SUPERIOR, LIMITE_INFERIOR, ITEMS } from './Config';


function App() {

  const [valor, setValor] = useState(LIMITE_INFERIOR);

  const menos = () => {
    if (valor > LIMITE_INFERIOR) {
      setValor(valor - 1);
    }
  };

  const mas = () => {
    if (valor < LIMITE_SUPERIOR-ITEMS + 1) {
      setValor(valor + 1);
    }
  };

  const botones = [];

  for (let i = 0; i<ITEMS; i++){
    botones.push(<Boton key={i} mensaje={valor+i} alerta={"alerta "+i} />)
  }
  
  // const elementos = [1,2,3];
  // const botones = elementos.map(el => <Boton key={el} mensaje={valor+el} />);
    
  return (
    <>
      <Boton accion={menos} mensaje="-" />
      {botones}
      <Boton accion={mas} mensaje="+" />
    </>

  );
}

export default App;
