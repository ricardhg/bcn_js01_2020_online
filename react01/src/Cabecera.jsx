
import React from 'react';

import "./css/cabecera.css";

/*
class Cabecera2 extends React.Component {
    render(){
        let estilosCabecera = {
            backgroundColor: this.props.color
        };
        return (
            <div className="cabecera" style={estilosCabecera} >{this.props.titulo}</div>
        );
    }
}
*/
function Cabecera(props){
    let estilosCabecera = {
        backgroundColor: props.color
    };
    return (
        <div className="cabecera" style={estilosCabecera} >{props.titulo}</div>
    );
}

export default Cabecera;