
import React from 'react';


class Listado extends React.Component {

    constructor(props){
        super(props);

        this.state = { 
            pokemons: []
        }

        this.cargaPokemons = this.cargaPokemons.bind(this);

        this.cargaPokemons();

    }


    cargaPokemons(){

        console.log("cargando...")

        fetch("http://localhost:3000/api/pokemons")
        .then( respuesta => respuesta.json())
        .then( datos =>{
             this.setState({pokemons: datos.data});
            })
        .catch( err => console.log(err));

    }




    render() {

        const filas = this.state.pokemons.map((poke, idx) => (
        <tr key={idx}>
            <td>{poke.id}</td>
            <td>{poke.nom}</td>
            <td>{poke.tipus}</td>
        </tr>
        ))

        return (
            <>
            <h1>Listado</h1>
            <table >
                <tbody>
                    {filas}
                </tbody>
            </table>


            </>
        );
    }


}

export default Listado;