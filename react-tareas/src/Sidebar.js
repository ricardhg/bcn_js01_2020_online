import React from "react";
import { Input, Button } from "reactstrap";

/*
Hacer que el boton de nueva tarea funcione
Mejorar presentación
Hacer que el input de color muestre selector de colores
...
*/

class SideBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      texto: '',
      color: '#ffffff'
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.nuevaTarea = this.nuevaTarea.bind(this);
  }

  handleInputChange(evento) {
    const target = evento.target;
    const value = (target.type === 'checkbox') ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  nuevaTarea() {
    //leer texto, color de state y llamar a this.props.nuevaTarea...
    this.props.nuevaTarea(this.state.texto, this.state.color);
    //console.log("creando tarea...");
  }



  render() {
    return (
      <>
        <h4>Nueva Tarea</h4>
        Tarea
        <Input type="text" name="texto" onChange={this.handleInputChange} />
        <br />
        Color
        <Input type="color" value={this.state.color} name="color" onChange={this.handleInputChange} />
        <br />
        <Button onClick={this.nuevaTarea} >Añadir</Button>
      </>
    )
  }

}

export default SideBar;
