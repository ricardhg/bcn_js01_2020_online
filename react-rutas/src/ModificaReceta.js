import React from 'react';
import Receta from './modelos/Receta';
import Producto from './modelos/Producto';
import Ingrediente from './modelos/Ingrediente';
import { Link } from 'react-router-dom';
import { Input, Row, Col, Table, Button } from 'reactstrap';



export default class ModificaReceta extends React.Component {

    constructor(props) {
        super(props);

        let receta = Receta.getRecetaById(this.props.match.params.idReceta * 1);
       
        //this.setState(receta);
        this.state = {
            id: receta.id,
            nombre: receta.nombre,
            descripcion: receta.descripcion
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.guardaReceta = this.guardaReceta.bind(this);

    }


    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    guardaReceta(){
        let nuevaDescripcion = this.state.descripcion;
        Receta.modificaReceta(this.state.id, nuevaDescripcion);
    }
                
    render() {
    

        return (
            <>

                <Row>
                    <Col>
                        <h2>Editando receta: {this.state.nombre}</h2>
                        <br />
                    </Col>
                </Row>
                <Row>
                   
                      
                    <Col >
                        <h5>Descripción:</h5>
                        <Input value={this.state.descripcion} 
                            onChange={this.handleInputChange} 
                            type="textarea" 
                            name="descripcion" 
                        />
                        <br />
                    {/*    <Button onClick={()=>this.guardaReceta()} >Guardar</Button> */}
                        <Link className="btn btn-success" onClick={()=>this.guardaReceta()} to="/ListaRecetas">Guardar</Link>

                      <br />
                      <br />
                        <Link to="/ListaRecetas">Volver al listado</Link>

                    </Col>
                </Row>



            </>
        )
    }

}