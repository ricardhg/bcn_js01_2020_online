import React, {useState} from 'react';
import styled from 'styled-components';

const Boton = styled.button`
    border: 2px solid red;
`;


export default () => {

    const [valor, setValor] = useState(1);


    const incrementa = (q) => setValor(valor + q);


    return (
        <>
            <Boton disabled={valor<=0} onClick={() => incrementa(-1)} > prev </Boton>
            <input value={valor} />
            <Boton disabled={valor>=15} onClick={() => incrementa(1)} > next </Boton>
        </>
    )
}