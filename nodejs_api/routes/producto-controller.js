const express = require('express');
const router = express.Router();

//requerimos el index.js de models que inicializa sequelize
const model = require('../models/index');

// importante: todas las rutas get, post… son relativas a la ruta principal
// de este controlador: /api/productos


// GET lista de todos los productos
// vinculamos la ruta /api/productos a la función declarada
// si todo ok devolveremos un objeto tipo:
//     {ok: true, data: [lista_de_objetos_producto...]}
// si se produce un error:
//     {ok: false, error: mensaje_de_error}

router.get('/', function (req, res, next) {
    //findAll es un método de sequelize!
    model.Producto.findAll( )
        .then(productos => res.json({
            ok: true,
            data: productos
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
    });

// GET de tipo /api/productos/7
// petición de UN producto, con ID=7
// "7" se recibe por parámetro y se lee con req.params.id
// en este caso utilizamos el método sequelize findOne
// con la condición "where id=7" 
// expresada en forma de objeto: { where: {id:req.params.id}}

router.get('/:id', function (req, res, next) {
    
    model.Producto.findOne({ where: {id: req.params.id}} )
        // .then(producto => producto.get({plain: true}))
        .then(producto => res.json({
            ok: true,
            data: producto
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
    });




// POST a /api/productos creamos un nuevo registro
// en primer lugar creamos un modelo Producto con el método sequelize "create"
// a partir de los datos presentes en el "body"
// a continuación, mediante "save", el objeto se guarda automáticamente
// en la base de datos y e actualiza su ID!
// igualmente se devuelve el objeto creado

router.post('/', function(req, res, next) {
    model.Producto.create(req.body)
    .then((item) => item.save())
    .then((item)=>res.json({ok: true, data:item}))
    .catch((error)=>res.json({ok:false, error}))
});
 


// PUT a /api/productos/X 
// en primer lugar se localiza el producto con id=X en la BDD
// a continuación, mediante "update", el objeto se actualiza con los datos
// presentes en el "body"
router.put('/:id', function(req, res, next) {
    model.Producto.findOne({ where: {id: req.params.id}} )
    .then((item)=>
        item.update(req.body)
    )
    .then((ret)=> res.json({
        ok: true,
        data: ret
    }))
    .catch(error => res.json({
        ok: false,
        error: error
    }));
});
 


// DELETE a /api/productos/X 
// se elimina el registro con id = X con elmétodo sequelize "destroy"
router.delete('/:id', function(req, res, next) {
    model.Producto.destroy({ where: {id: req.params.id}} )
    .then((data)=>res.json({ok: true, data}))
    .catch((error)=>res.json({ok:false, error}))
});


module.exports = router;
