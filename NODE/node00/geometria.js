
function areaT(b, h){
    return b*h/2;
}

const areaC = (radio) => radio*radio*3.141527;

const geometria = {areaT, areaC};

module.exports = geometria;
