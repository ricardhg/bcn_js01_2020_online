import React from "react";


function Producto(props){

  return (
    <div className="fruta">
      <p>Nombre: {props.fruta.nom}</p>
      <p>Precio: {props.fruta.preu}</p>
      <button onClick={()=>props.anyadir(props.fruta)}>Añadir</button>

    </div>
  )

}




class ListaProductos extends React.Component {
  

  render(){

    const listado = this.props.productos.map( (el, index) => <Producto anyadir={this.props.anyadir} key={index} fruta={el} />);

    return (
      <div>
        <h4>productos</h4>

      {listado}
        

      </div>
    )


  }

}

export default ListaProductos;