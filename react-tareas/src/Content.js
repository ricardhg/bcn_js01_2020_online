import React from "react";
import { Button } from "reactstrap";


/*
 añadir key a map, mejorar presentación poniendo color de fondo en cada tarea,
 hacer que el botón eliminaTarea funcione correctamente y reemplazarlo por icono "fa-trash"
*/

function Content(props) {

    let tareas = props.tareas.map((el,index) => 
        <div key={index} className="tarea" style={{backgroundColor: el.color}} >
            <p>{el.texto} {el.id}</p>
            <Button onClick={()=> props.eliminaTarea(el.id)} >Eliminar</Button>
            <Button onClick={()=> props.modificaTarea(el)} >Modificar</Button>
        </div>
        )

    return (
        <>
            <h4>Contenido</h4>
            {tareas}
        </>
    )
}

export default Content;
