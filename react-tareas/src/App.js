import React from "react";
import { Container, Row, Col } from "reactstrap";
import './css/app.css';

import Header from './Header';
import SideBar from './Sidebar';
import ModificaBar from './ModificaBar';
import Content from './Content';

const TAREAS_DEMO = [
  {
    id: 1,
    texto: "Comprar mascarilla",
    color: "#ff0000"
  },
  {
    id: 2,
    texto: "Hacer ejercicio TAREAS",
    color: "#00ff00"
  },
  {
    id: 3,
    texto: "Jugar GTA",
    color: "#0000ff"
  },

];



class App extends React.Component {

  constructor(props) {
    super(props);

     
    let tareasGuardadas = localStorage.getItem('__tareas__') ;
    

    this.state = {
      tareas: (tareasGuardadas===null)
              ? TAREAS_DEMO
              : JSON.parse(tareasGuardadas),
      modificando: false,
      idM: '',
      textoM: '',
      colorM:''
    };

    this.eliminaTarea = this.eliminaTarea.bind(this)
    this.modificaTarea = this.modificaTarea.bind(this)
    this.nuevaTarea = this.nuevaTarea.bind(this)
    this.cambiarTarea = this.cambiarTarea.bind(this)
    this.guardar = this.guardar.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    // this.recuperar = this.recuperar.bind(this)

    //this.recuperar();
  }


  handleInputChange(evento) {
    const target = evento.target;
    const value = (target.type === 'checkbox') ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }


  guardar(){
    localStorage.setItem('__tareas__', JSON.stringify(this.state.tareas));
    
    }

    // recuperar(){

    //     let tareasGuardadas = JSON.parse( localStorage.getItem('__tareas__') );
    //     if (tareasGuardadas!==null){
    //       this.setState({tareas: tareasGuardadas});
    //     }
    // }


  nuevaTarea(texto, color) {
    
    // asignar id y añadir tarea a lista de tareas del state
    let tareas2 = JSON.parse(JSON.stringify(this.state.tareas));
    
    let siguienteId = 0;
    tareas2.forEach (el => {
      if (el.id>siguienteId) {
        siguienteId = el.id;
      }
    })
    siguienteId++;


    // let siguienteId = this.state.tareas.length +1;
    let tareaNueva = {id:siguienteId, texto:texto, color:color};
    tareas2.push(tareaNueva);

    this.setState({tareas: tareas2}, this.guardar);

  }

  
  asignarId() {
    let id = this.state.tareas.length;
    let existe = false;
    do {
      existe = false;
      this.state.tareas.map(el => {
        if (el.id === id) {
          existe = true;
          id++;
        }
      });
    } while (existe);

    return id;

  }

  eliminaTarea(idBorrar) {
    // eliminar tarea con id facilitado
    let tareas2 = JSON.parse(JSON.stringify(this.state.tareas));
    tareas2  = tareas2.filter(el => el.id!==idBorrar);
    this.setState({tareas: tareas2}, this.guardar);
  }

  modificaTarea(tarea) {
    console.log("modificando id " + tarea.id);
    this.setState({
              modificando: true,
              idM: tarea.id,
              textoM: tarea.texto,
              colorM: tarea.color} );
  }

  cambiarTarea(){
    let texto_m = this.state.textoM;
    let color_m = this.state.colorM;

    let tareas2 = JSON.parse(JSON.stringify(this.state.tareas));

    tareas2 = tareas2.map(el => {
        if(el.id === this.state.idM){
          el.texto = texto_m;
          el.color = color_m;
        }
        return el;
    })

    this.setState({tareas: tareas2, modificando: false}, this.guardar);
  }

  render() {


    return (
      <>
        <Header cargarTareas={this.recuperar} />
        <Container>
          <Row>

            { (this.state.modificando===true)
              ? (
                <Col xs="3" className="modificabar">
                  <ModificaBar handleInputChange={this.handleInputChange}
                   cambiarTarea={this.cambiarTarea}
                    textoM={this.state.textoM}
                     colorM={this.state.colorM} />
                </Col>
              )
              : (
                <Col xs="3" className="sidebar">
                  <SideBar nuevaTarea={this.nuevaTarea} />
                </Col>
              )
            }

            <Col className="content">
              <Content tareas={this.state.tareas} eliminaTarea={this.eliminaTarea} modificaTarea={this.modificaTarea}/>
            </Col>
          </Row>
        </Container>
      </>
    );

  }

}

export default App;
