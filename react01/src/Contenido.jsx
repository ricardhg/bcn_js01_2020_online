import React from 'react';
import Bola from './Bola';

function TituloRojo(props){

    return (
        <h1 style={{color: "red"}}>{props.children}</h1>
    );
}


function Contenido(props){


    return(
        <>
            <TituloRojo>Lorem, ipsum dolor.</TituloRojo>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit eveniet earum deleniti dolore vel labore, veritatis est excepturi. Dolore saepe vel itaque dicta ducimus porro eum impedit facere tenetur dolorum!</p>

            <h1>Capítulo 2</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit eveniet earum deleniti dolore vel labore, veritatis est excepturi. Dolore saepe vel itaque dicta ducimus porro eum impedit facere tenetur dolorum!</p>


            <Bola />

        </>
    );
}

export default Contenido;