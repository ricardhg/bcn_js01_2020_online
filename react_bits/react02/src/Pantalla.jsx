import React from "react";
import {Alert} from "reactstrap";

function Pantalla (props){
    return (
        <Alert color="success">
            {props.texto}
        </Alert>
    )
}

export default Pantalla;