import React from 'react';
// import './css/interruptor.css';


export default class Interruptor extends React.Component{
    
    constructor(props){
        super(props);

        this.state = {
            on : true
        }

        this.pulsar = this.pulsar.bind(this);
    }
    


    pulsar(){
        //this.state = { on: !this.state.on};

        this.setState({
            on: !this.state.on
        })
    }

    render(){
        let claseIcono = (this.state.on) ? "fa fa-toggle-on" : "fa fa-toggle-off" ;

        /*
        let claseIcono;
        if (this.state.on === true){
            claseIcono = "fas fa-toggle-on";
        }else{
            claseIcono = "fas fa-toggle-off";
        }
        */

        let estadoIcono = (this.state.on) ? "ACTIVO"  :"INACTIVO";

        return (
            <div onClick={this.pulsar} >
                <i className={claseIcono} />
                <p>{estadoIcono}</p>
            </div>
        );
    }

}
