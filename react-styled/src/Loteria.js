import React from 'react';
import {Container, Button} from 'reactstrap';
import styled from 'styled-components';

//import './loteria.css';

const Titulo = styled.h1`
color: red;
text-decoration: underline;
`;

const Bola = styled.div`
height: 40px;
width: 40px;
display: inline-block;
font-size: 30px;
line-height: 40px;
background-color: ${x => x.color};
color: white;
text-align: center;
border-radius: 40px;
margin: 5px;
`;


export default class Loteria extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            numeros: [],
            color: "black"
        }

        this.clicar = this.clicar.bind(this);
        this.borrar = this.borrar.bind(this);
        this.guardar = this.guardar.bind(this);
        this.recuperar = this.recuperar.bind(this);
    }


    borrar(){
        this.setState({numeros: []});
    }

    guardar(){
        localStorage.setItem('loteria', JSON.stringify(this.state.numeros));
        
    }

    recuperar(){
        let numerosGuardados = JSON.parse( localStorage.getItem('loteria') );
        this.setState({numeros: numerosGuardados});
    }

    clicar(){
        let nuevoNumero = Math.floor(Math.random()*9);

        // MAL: let nuevaLista = this.state.numeros;
        
        let nuevaLista = [...this.state.numeros];
        nuevaLista.push(nuevoNumero);

        this.setState({numeros: nuevaLista});
    }

    render(){

        let bolas = this.state.numeros.map( (el,index) => <Bola color={this.state.color} key={index}>{el}</Bola>)

        return (
            <Container>
                <br />
                <Titulo>Loteria</Titulo>
                <h1>Hola que tal</h1>
                <br />
                <Button onClick={this.clicar} >Bola</Button>
                <Button onClick={this.guardar} >Guardar</Button>
                <Button onClick={this.recuperar} >Recuperar</Button>
                <Button onClick={this.borrar} >Borrar</Button>
                <br />
                <br />
                {bolas}
                <br />
            </Container>
        )
    }



}