
const express = require('express');

const app = express();

app.use(express.urlencoded({ extended: true }));


app.get('/', function (req, res) {
    res.send('Hola mundo!')
})

app.get('/adios', function (req, res) {
    res.send('<h1>Adiós mundo cruel!</h1>')
})

app.get('/moto', (req, res) => {

    const motos = [
        {
            id: 1,
            marca: 'honda',
            modelo: 'cb 500',
            potencia: '47cv'
        },
        {
            id: 2,
            marca: 'honda',
            modelo: 'scoopy 125',
            potencia: '15cv'
        }
    ];

    const idSolicitado = req.query.id * 1;
    let laMoto = motos.filter(el => el.id===idSolicitado)[0];

    res.json(laMoto);
})

// http://localhost:3000/saluda?nombre=Araceli
app.get('/saluda', function (req, res) {
    const nombre = req.query.nombre;
    res.send('Hola ' + nombre + ", como estás?")
})

app.get('/login', function (req, res) {
    const nombre = req.query.user;
    const clave = req.query.password;
    if (nombre === 'root' && clave === 'admin') {
        res.send('Login OK');
    } else {
        res.send('Acceso denegado');
    }
})

app.post('/login', function (req, res) {
    const nombre = req.body.user;
    const clave = req.body.password;
    if (nombre === 'root' && clave === 'admin') {
        res.send('Login Post OK');
    } else {
        res.send('Acceso Post denegado');
    }
})


app.listen(3000, function () {
    console.log('Escuchando en puerto 3000!')
})


