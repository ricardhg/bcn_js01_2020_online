module.exports = (sequelize, DataTypes) => {
    const Pokemon = sequelize.define('Pokemon', {
  
      nom: DataTypes.STRING,
      tipus: DataTypes.STRING,
      
    }, { tableName: 'pokemons'});
    
    return Pokemon;
  };
  