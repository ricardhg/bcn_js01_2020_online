import React from "react";
import "./gato.css";

export const GATOS = 2;

 function Gato200(){
  return(
      <img className="gato" src="http://placekitten.com/200/200" />
  );
}


 function Gato300(){
    return(
        <img src="http://placekitten.com/300/300" />
    );
  }

  export {Gato200, Gato300};

  export default function Gato(props){
      let url = `http://placekitten.com/${props.ancho}/${props.alto}`;
    return(
        <img src={url} />
    );
  }
