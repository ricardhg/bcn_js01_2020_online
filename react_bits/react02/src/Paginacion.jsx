import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';


class Paginacion extends React.Component {

    constructor(props){
        super(props);


        this.state = {
            inicio: 7
        }

        this.anterior = this.anterior.bind(this);
        this.siguiente = this.siguiente.bind(this);
        this.principio = this.principio.bind(this);
        this.final = this.final.bind(this);
      
    }

    anterior(){
        if (this.state.inicio===1) {
            return;
        }
        
        let valorActual = this.state.inicio;
        let valorNuevo = valorActual -1;
        let nuevoState = { inicio: valorNuevo};
        this.setState(nuevoState);
    }

    siguiente(){
        if (this.state.inicio===16) {
            return;
        }
        this.setState( { inicio: this.state.inicio+1 });
    }

    principio(){
        this.setState({inicio: 1});
    }

    final(){
        this.setState({inicio: 16});
    }


    render() {

        return (
            <Pagination aria-label="Page navigation example">
                <PaginationItem>
                    <PaginationLink first onClick={this.principio}  />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink previous onClick={this.anterior} />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">
                        {this.state.inicio}
        </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">
                    {this.state.inicio+1}
        </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">
                    {this.state.inicio+2}
        </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">
                    {this.state.inicio+3}
        </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">
                    {this.state.inicio+4}
        </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink next onClick={this.siguiente} />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink last onClick={this.final} />
                </PaginationItem>
            </Pagination>
        );
    }
}

export default Paginacion;


