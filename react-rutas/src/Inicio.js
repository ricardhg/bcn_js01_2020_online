
import React, {useEffect, useState} from 'react';
import {Button} from 'reactstrap';


const horaActual = () => (new Date()).toLocaleString().split(' ')[1];

export default () => {
    const [hora, setHora] = useState(horaActual());
    const actualiza = () => setHora(horaActual());
    useEffect(()=>{
            const  intervalo = setInterval(actualiza, 1000);
            return ()=>clearInterval(intervalo);
            }, []);
    return (
        <>
        {hora}
        <br />
        <br />
        <Boton />
        </>
    )
}





const Boton = () => {

    const [colorBoton, setColorBoton] = useState('success');

    //function cambiaColor() {

    const cambiaColor = () => {
        const nuevoColor = (colorBoton==='success') ? 'danger' : 'success';
        setColorBoton(nuevoColor);
    }
   
    return (
        <Button color={colorBoton} onClick={cambiaColor} >Hola</Button>
    )
}

