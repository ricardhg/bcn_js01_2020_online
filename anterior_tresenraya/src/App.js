import React from "react";
import { Container, Alert, Row, Col } from 'reactstrap';
import Boton from './Boton';


class App extends React.Component {

  constructor(props){

    super(props);
    this.state={
      turno: "o",
      mapa: [0,0,0, 0,0,0, 0,0,0],
      winner: false
    }

    this.cambiaTurno = this.cambiaTurno.bind(this);
    this.analizaMapa = this.analizaMapa.bind(this);

  }


  analizaMapa(){
      console.log("analizando jugadas...");
      
  }

  cambiaTurno(boton, jugada){
    let nuevoTurno = (this.state.turno==="x") ? "o" : "x";
    let posicion = boton-1;
    jugada = (jugada==="o") ? 1 : 2;
    let nuevoMapa = [...this.state.mapa];
    nuevoMapa[posicion]=jugada;
    this.setState({mapa: nuevoMapa, turno: nuevoTurno}, this.analizaMapa );
   
  }

  render() {


    let prueba = "";
    this.state.mapa.forEach(el => prueba=prueba+el)
 
    return (
      <Container>
        <h1>3 en raya</h1>
        <Row>
          <Col>
            <Boton orden={1} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
          <Col>
            <Boton orden={2} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
          <Col>
            <Boton orden={3} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
        </Row>
        
        <Row>
          <Col>
            <Boton orden={4} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
          <Col>
            <Boton orden={5} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
          <Col>
            <Boton orden={6} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
        </Row>

        <Row>
          <Col>
            <Boton orden={7} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
          <Col>
            <Boton orden={8} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
          <Col>
            <Boton orden={9} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
          </Col>
        </Row>
      <p>{prueba}</p>

      </Container>
    );
  }
}


export default App;

