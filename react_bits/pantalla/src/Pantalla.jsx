import React from 'react';
import {Alert} from 'reactstrap';


export default function Pantalla(props){
    return (
        <Alert color="success">
            {props.texto}
        </Alert>
    );
}

