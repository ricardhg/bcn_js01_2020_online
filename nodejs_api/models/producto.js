module.exports = (sequelize, DataTypes) => {
    const Producto = sequelize.define('Producto', {
  
      nombre: DataTypes.STRING,
      precio: DataTypes.INTEGER,
      
    }, { tableName: 'productos'});
    
    return Producto;
  };
  