import React from "react";

import "./css/gato.css";


class Gato extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            fondo: "green",
            cosa: "gato azul"
        }

        this.cambiaFondo = this.cambiaFondo.bind(this);

    }


    cambiaFondo(){
        let nuevoState = {
            fondo: "red"
        }
        this.setState(nuevoState);
    }

    render() {
        let urlGato = "http://placekitten.com/"
            + this.props.ancho
            + "/" + this.props.alto;
        
        let estilo = {
            backgroundColor: this.state.fondo
        };

        return (
            <>
                <div className="caja-gato" style={estilo} >
                    <img src={urlGato} alt="Mi gato" />
                    <p>{this.props.nombre}</p>
                </div>
                <br/>
                <br/>
                <button onClick={this.cambiaFondo}>Cambiar fondo</button>
            </>
        );
    }
}

export default Gato;


/*
function Gato(props) {
    let urlGato = "http://placekitten.com/"
        + props.ancho
        + "/" + props.alto;
    return (
        <div className="caja-gato" >
            <img src={urlGato} alt="Mi gato" />
            <p>{props.nombre}</p>
        </div>
    );
}
*/

