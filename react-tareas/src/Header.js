import React from "react";
import {Container} from 'reactstrap';
 

function Header(){

    return (
        <Container fluid className="header">

            <Container>
                
                <h1><i className="fa fa-address-book-o " aria-hidden="true"></i> Tareas</h1>
            </Container>

        </Container>

    )
}

export default Header;
