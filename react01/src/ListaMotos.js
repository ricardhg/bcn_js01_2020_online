import React from 'react';
import {MOTOS} from './motos.js';

class ListaMotos extends React.Component {

    constructor(props) {
        super(props);
        this.creaFila = this.creaFila.bind(this);
    }

    creaFila(moto,index){
        return (
            <tr key={index}>
                <td>{moto.marca}</td>
                <td>{moto.model}</td>
                <td>{moto.eur}</td>
                <td>{moto.km}</td>
            </tr>
        );
    }

    honda(moto){
        // if(moto.marca==="HONDA"){
        //     return true;
        // } else {
        //     return false;
        // }

        return moto.marca==="HONDA";
    }

    comparaKm(moto1,moto2){
        let x = 0;
        
        if (moto1.km > moto2.km){
            x=1;
        } else if (moto1.km < moto2.km) {
            x=-1;
        } else {
            x = 0;
        }

        return x ; // 1,0,-1
    }

    comparaPreu(moto1,moto2){
        let x = 0;
        
        if (moto1.eur > moto2.eur){
            x=-1;
        } else if (moto1.eur < moto2.eur) {
            x=1;
        } else {
            x = 0;
        }

        return x ; // 1,0,-1
    }

   //   moto => moto.marca==="HONDA"

    render(){

       // versión función externa: let filas = MOTOS.filter(this.honda).map(this.creaFila);
       // versión arrow: let filas = MOTOS.filter(moto => moto.marca==="YAMAHA").map(this.creaFila);
      /* versión con function:
       let filas = MOTOS.filter(
                function(moto){
                     return moto.marca==="HONDA";
                    }
           ).map(this.creaFila);
                */
                  
       let filas = MOTOS
                    .filter(moto => moto.marca==="YAMAHA")
                    .sort(this.comparaPreu)
                    .map(this.creaFila);

        let fila = filas[0];
        let filas10 = filas.splice(0,10);

        return(
            <>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Marca</th>
                            <th>Model</th>
                            <th>Preu</th>
                            <th>Km</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas10}
                    </tbody>
                </table>
            </>
        );
    }

}

export default ListaMotos;
