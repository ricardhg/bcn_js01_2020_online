import React from 'react';
import { Button } from 'reactstrap';


class Boton extends React.Component {

    constructor(props) {
        super(props);

        this.clicar = this.clicar.bind(this);
    }

    clicar() {
        this.props.clicar(this.props.valor);
    }

    render() {

        return (
            <Button block color="warning" onClick={this.clicar} >{this.props.valor} </Button>
        );
    }
}


export default Boton;