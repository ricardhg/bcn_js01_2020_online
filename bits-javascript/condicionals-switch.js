
let dia = 8;
let mensaje = diaSemana(dia);
console.log(mensaje);


function diaSemana(diaNumerico){
  let respuesta = "nada";
  
  switch(diaNumerico){
    case 1:
      respuesta="lunes";
      break;
    case 2:
      respuesta="martes";
      break;
    case 3:
      respuesta="miércoles";
      break;
    case 4:
      respuesta="jueves";
      break;
    case 5:
      respuesta="viernes";
      break;
    case 6:
      respuesta="sábado";
      break;
    case 7:
      respuesta="domingo";
      break;
    default:
      respuesta="error, dia debería estar entre 1 y 7"
      break;
  }
  
  
  return respuesta;
}



function diaSemana2(diaNumerico){
  let respuesta = "nada";
  
  if(diaNumerico==1){
    respuesta= "lunes";
  } else if(diaNumerico==2){
    respuesta = "martes";
  } else if(diaNumerico==3){
    respuesta = "miércoles";
  }
  
  return respuesta;
}



