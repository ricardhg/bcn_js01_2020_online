import React from "react";

import ListaProductos from './ListaProductos';
import ListaCompra from './ListaCompra';

import './app.css';

const PRODUCTES = [
  {
      "id" : 1,
      "nom" : "Plàtan",
      "preu" : 0.5
  },
  {
      "id" : 2,
      "nom" : "Poma",
      "preu": 0.8
  },
  {
      "id" : 3,
      "nom" : "Pinya",
      "preu": 5
  },
  {
      "id" : 4,
      "nom" : "Meló",
      "preu": 6
  },
];

class App extends React.Component {
  constructor(props){
    super(props);

    this.state={
      carrito: []
    }

    this.anyadir = this.anyadir.bind(this);

  }


  anyadir(producto){
    let nuevo = true;
    let nuevoCarrito = this.state.carrito.map( el => {
      
        if(producto.id === el.id){
           el.unidades = el.unidades+1;
           nuevo = false;
        }
        return el;
    })
    if (nuevo===true){
      producto.unidades=1;
      nuevoCarrito.push(producto);
    }
    
    this.setState({carrito: nuevoCarrito});
  }


  render(){

    return (
      <>
        <h1>Fruteria</h1>
        <div className="contenedor" >
          <ListaProductos anyadir = {this.anyadir} productos={PRODUCTES} />
          <ListaCompra carrito={this.state.carrito} />
        </div>

      </>
    )


  }

}

export default App;