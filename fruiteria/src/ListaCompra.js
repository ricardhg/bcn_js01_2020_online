import React from "react";

function Seleccionado(props){
  return (
    <div className="frutaSeleccionada">
      <p>Nombre: {props.fruta.nom}</p>
      <p>Precio: {props.fruta.preu}</p>
      <p>Unidades: {props.fruta.unidades} </p>
    </div>
  )
}

class ListaCompra extends React.Component {
  constructor(props){
    super(props);
  }

  render(){

     const compras = this.props.carrito.map( (el, index) => <Seleccionado key={index} fruta={el} />);

     let total = 0;
     this.props.carrito.forEach( el => {
       let precio = el.unidades*el.preu;
       total = total + precio;
     })

    return (
      <div>
      
      <h4>Compra</h4>
       {compras}
       <p>TOTAL: {total}</p>
      </div>
    )


  }

}

export default ListaCompra;