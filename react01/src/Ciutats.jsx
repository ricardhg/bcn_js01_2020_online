import React from "react";

import {CIUTATS} from "./datos.js";



function Lista(props){
    let ciudadesLi = props.items.map( (el, indice) => <li key={indice}>{el}</li>)
    return (
        <ul>
            {ciudadesLi}
        </ul>
    )
}




class Ciutats extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
     
        let ciudadesLi = CIUTATS
                            .filter(el => (el.toLowerCase())[0]==='s')
                            .map( (el, indice) => <li key={indice}>{el}</li>)

        let filas = CIUTATS.filter(el => (el.toLowerCase())[0]==='s')
                            .map( (el,index) => <tr key={index}><td>{index}</td><td>{el}</td></tr>)              

        return (
            <>
                <h1>Ciutats</h1>

                <Lista items={CIUTATS} />

                <br />
                <br />
                <br />

                <table>
                    <thead>
                        <tr>
                            <th>num</th>
                            <th>Ciutat</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </table>
            </>
        )
    }
}

export default Ciutats;