import React from 'react';



function Bola(props) {
    let color = "";
    switch (props.color) {
        case 1:
            color = "red"
            break;
        case 2:
            color = "blue"
            break;
        case 3:
            color = "green"
            break;
        default:
            break;
    }

    return (
        <h1 onClick={props.clicar2} style={{color: color}}>HOLA</h1>
    );

}

class Tricolor extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            color: 1
        };

        this.clicar = this.clicar.bind(this);
    }

    clicar() {
        console.log("se ha clicado")
        if (this.state.color === 3) {
            this.setState({ color: 1 });
        } else {
            this.setState({ color: this.state.color + 1 })
        }
    }

    render() {
        return (
            <Bola clicar2={this.clicar} color={this.state.color} />
        )
    }


}

export default Tricolor;