const express = require('express');
const app = express();

//middleware necessari per poder llegir body en post
app.use(express.urlencoded({ extended: false }));

//carpeta per defecte
app.use(express.static('public'))

//ruta principal, mostra vista index.ejs passant-li les variables
app.get('/', function(req, res) { 
    const vars = {titol: 'Inici',  img: 'avatar1.jpg'}
    res.render('index.ejs', vars);
});

//ruta principal, mostra vista index.ejs passant-li les variables
app.get('/ciutats', function(req,res) {
    const vars = {titol: 'Inici', ciutats: ['barcelona', 'girona', 'vic', 'puigcerdà']};
    res.render('ciutats.ejs', vars);
})


//aquest use es genèric, el definim al final
//si no hi ha hagut cap coincidencia amb les rutes anteriors reenviem a /todo
app.use(function(req, res, next){
    res.redirect('/');
});


//inciem "escolta" en port 3000
app.listen(3000, function () {
    console.log('App a http://localhost:3000')
  });
  
