import React from 'react';

// export default () => <img src="http://lorempixel.com/100/100" />;


export const Gato = () => <img src="http://placekitten.com/200/200" />;


const Logo = () => <h1>Logotipo</h1>;
export default Logo;



/*
equivalente a:

const Logo = () => <img src="http://lorempixel.com/100/100" />;
export default Logo;

o:

function Logo(){
return <img src="http://lorempixel.com/100/100" />;
}
export default Logo;

*/
