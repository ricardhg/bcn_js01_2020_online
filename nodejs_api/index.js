//importamos/requerimos express y controladores
const express = require('express');
const cors = require('cors')

const pokemonsRouter = require('./routes/pokemon-controller');
const productosRouter = require('./routes/producto-controller');
const indexRouter = require('./routes/index-controller');


//instanciamos nueva aplicación express
const app = express();

app.use(cors());

//necesario para poder recibir datos en json
app.use(express.json());

//las ruta "/" se gestiona en indexRouter
app.use('/', indexRouter);
//las rutas que empiecen por /api/pokemons se dirigirán a pokemonsRouter
app.use('/api/pokemons', pokemonsRouter);

app.use('/api/productos', productosRouter);

//arranque del servidor
const port = 3000
app.listen(port, () => console.log(`App listening on port ${port}!`))
