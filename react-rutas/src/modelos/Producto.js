

let CLAVE_LOCAL_STORAGE = "recetario_productos";

class Producto {


    static guardar(datos) {
        localStorage.setItem(CLAVE_LOCAL_STORAGE, JSON.stringify(datos));
    }

    static recuperar() {
        let datos = JSON.parse(localStorage.getItem(CLAVE_LOCAL_STORAGE));
        if (datos === null) {
            return [];
        } else {
            return datos;
        }
    }


    static getProductos = () => {

        // leer de local storage
        return this.recuperar()
    }

    static getProductoById = (idProducto) => {
        //leer lista de local storage y guardar en PRODUCTOS
        let PRODUCTOS = this.recuperar();

        let elProducto = PRODUCTOS.find(el => el.id === idProducto);
        return elProducto;
    }

    static addProducto = (nombreProducto) => {
        let PRODUCTOS = this.recuperar();
        let max = 0;
        PRODUCTOS.forEach(el => {
            max = (max < el.id) ? el.id : max;
        })
        let nuevoId = max + 1;
        let productoNuevo = { id: nuevoId, nombre: nombreProducto };
        PRODUCTOS.push(productoNuevo);
        this.guardar(PRODUCTOS);

    }

    static eliminaProducto = (idBorrar) => {
        let datos = this.recuperar();
        datos = datos.filter(el => el.id !== idBorrar * 1);
        this.guardar(datos);
    }
}

export default Producto;