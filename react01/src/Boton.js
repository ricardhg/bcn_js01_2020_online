import React from 'react';

function Boton(props) {
    let textoBoton =  (props.encendido===true) ? "Apagar" : "Encender" ;

    return(
         <button onClick={props.pulsar} >{textoBoton}</button>
         );
}

export default Boton;
