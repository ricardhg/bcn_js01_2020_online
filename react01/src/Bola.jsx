import React from 'react';
import "./css/bola.css";

// function Bola(){
//     return (
//         <div className="bola" />
//     )
// }


class Bola extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            colorBola: "green"
        }
        this.cambia = this.cambia.bind(this);
    }

    cambia(){
        let color="red";
        if (this.state.colorBola==="red"){
            color = "green";
        }
        this.setState({colorBola: color});
    }

    render(){
        //...
        return (
            <div className="bola" onClick={this.cambia}
             style={{backgroundColor: this.state.colorBola}} />
        );
    }
}

export default Bola;