const express = require('express');
const router = express.Router();

//requerimos el index.js de models que inicializa sequelize
const model = require('../models/index');


// ANTES SIEMPRE HABRÁ: /api/pokemons/...

// GET lista de todos los pokemons
// vinculamos la ruta /api/pokemons a la función declarada
// si todo ok devolveremos un objeto tipo:
//     {ok: true, data: [lista_de_objetos_pokemon...]}
// si se produce un error:
//     {ok: false, error: mensaje_de_error}

router.get('/', function (req, res, next) {
    //findAll es un método de sequelize!
    model.Pokemon.findAll( )
        .then(pokemons => res.json({
            ok: true,
            data: pokemons
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
    });
 


module.exports = router;