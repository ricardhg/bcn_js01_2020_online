import React, {useState} from 'react';
import { Tooltip } from 'reactstrap';



export default (props) => {
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);

    return (
        <>
        <button id="TooltipExample" className="btn" onClick={props.accion} >{props.mensaje}</button>
        <Tooltip placement="right" isOpen={tooltipOpen} target="TooltipExample" toggle={toggle}>
        {props.alerta}
        </Tooltip>
        </>
    )
}