import React from "react";
import { Input, Button } from "reactstrap";

/*
Hacer que el boton de nueva tarea funcione
Mejorar presentación
Hacer que el input de color muestre selector de colores
...
*/

class ModificaBar extends React.Component {

  constructor(props) {
    super(props);

    this.cambiarTarea = this.cambiarTarea.bind(this);
  }



  cambiarTarea() {
    //leer texto, color de state y llamar a this.props.cambiarTarea...
    this.props.cambiarTarea();
    //console.log("creando tarea...");
  }



  render() {
    return (
      <>
        <h4>Modifica Tarea</h4>
        Tarea
        <Input type="text" value={this.props.textoM} name="textoM" onChange={(e)=>this.props.handleInputChange(e)} />
        <br />
        Color
        <Input type="color" value={this.props.colorM} name="colorM" onChange={(e)=>this.props.handleInputChange(e)} />
        <br />
        <Button onClick={this.cambiarTarea} >Guardar</Button>
      </>
    )
  }

}

export default ModificaBar;
