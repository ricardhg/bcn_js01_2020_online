import React from 'react';
import {Button} from 'reactstrap';


export default function Boton(props){
    return (
        <Button block color="warning" onClick={()=>props.clicar(props.texto)}>
            {props.texto}
        </Button>
    );
}

