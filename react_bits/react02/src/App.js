import React from "react";

import { Container, Row, Col, Alert, Button } from 'reactstrap';
import Pantalla from "./Pantalla";
import Boton from "./Boton";


class App extends React.Component {

  constructor(props) {
    super(props);

    this.state={
      texto: "Selecciona vehículo..."
    }
    this.clicar = this.clicar.bind(this);
  }


  clicar(x){
    this.setState({texto: x});
  }


  render() {

    return (
      <Container>
        <br/>
        <br/>
        <Row>
          <Col>
            <Pantalla texto={this.state.texto} />
          </Col>
        </Row>

        <Row>
          <Col>
            <Boton valor="Bici" clicar={this.clicar} />
          </Col>
          <Col>
            <Boton valor="Moto" clicar={this.clicar} />
          </Col>
          <Col>
            <Boton valor="Coche" clicar={this.clicar} />
          </Col>
        </Row>


      </Container>
    );
  }
}


export default App;