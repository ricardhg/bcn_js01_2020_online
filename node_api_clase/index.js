//importamos/requerimos express y controladores
const express = require('express');

const pokemonsRouter = require('./routes/pokemon-controller');
const indexRouter = require('./routes/index-controller');

//instanciamos nueva aplicación express
const app = express();

//necesario para poder recibir datos en json
app.use(express.json());

//las ruta "/" se gestiona en indexRouter
app.use('/', indexRouter);

//las rutas que empiecen por /api/pokemons se dirigirán a pokemonsRouter
app.use('/api/pokemons', pokemonsRouter);

//arranque del servidor
const port = 3000
app.listen(port, () => console.log(`App listening on port ${port}!`))
