
let numeros = [1,2,4,9,12];
let dies = ["Dilluns","Dimarts","Dimecres",
            "Dijous","Divendres", "Dissabte", "Diumenge"];
/*
for(let i = 0; i<dies.length; i=i+1){
  console.log(dies[i]);
}

for(i in dies){
  console.log(i);
  console.log(dies[i]);
}
*/

dies.forEach(escribir);
numeros.forEach(escribirDoble);

function escribir(algo){
  console.log(algo);
}

function escribirDoble(algo){
  console.log("El doble de "+algo+" es "+(algo*2));
}