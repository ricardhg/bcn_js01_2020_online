import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container , FormFeedback, FormText} from 'reactstrap';





class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            nombre: "",
            password: "",
            passwordStatus: 0,
            nombreStatus: 0
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleInputChange(evento) {
        const target = evento.target;
        //const value = (target.type === 'checkbox') ? target.checked : target.value;
        const value = target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
      }
    
      handleSubmit(event) {
        event.preventDefault();
        const NOM = 'ricard';
        const PASS = '1234';
        if (this.state.password===PASS){
            this.setState({passwordStatus: 1});
        } else {
            this.setState({passwordStatus: 2});
        }
        if (this.state.nombre===NOM){
            this.setState({nombreStatus: 1});
        } else {
            this.setState({nombreStatus: 2});
        }
        //... conexión con el servidor para enviar datos o consultar permisos...
        // fetch...
      }


    render() {

        let mensajeErrorPassword = null;
        if(this.state.errorPassword===true){
            mensajeErrorPassword=<h2>Error! password incorrecto</h2>;
        }

        let validPassword = null;
        if (this.state.passwordStatus===2){
            validPassword={invalid:true};
        } 

        if (this.state.passwordStatus===1) validPassword={valid:true};

        let validNombre = null;
        if (this.state.nombreStatus===2) validNombre={invalid:true};
        if (this.state.nombreStatus===1) validNombre={valid:true};


        return (
            <>
            <Container>
                <br />
                <br />
                <h2>Login Form</h2>
                <br />

                <Form  onSubmit={this.handleSubmit} >
                    <FormGroup>
                        <Label for="campoNombre">Nombre</Label>
                        <Input {...validNombre}  onChange={this.handleInputChange} type="text" name="nombre" id="campoNombre" placeholder="entra nombre" />
                        <FormFeedback valid>Ok! bienvenido ricard</FormFeedback>
                        <FormFeedback>Nombre incorrecto</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for="campoPassword">Password</Label>
                        <Input {...validPassword} onChange={this.handleInputChange} type="password" name="password" id="campoPassword" placeholder="entra password" />
                        <FormFeedback valid>Password ok</FormFeedback>
                        <FormFeedback>Password incorrecto</FormFeedback>
                    </FormGroup>
                    <Button>Enviar</Button>
                </Form>

            </Container>
            </>
        );
    }

}



export default Login;