const express = require('express');
const app = express();

const lista_ciudades = [
    {nombre: "barcelona", comunidad: "catalunya"},
    {nombre: "girona", comunidad: "catalunya"},
    {nombre: "vic", comunidad: "catalunya"},
    {nombre: "lleida", comunidad: "catalunya"},
    {nombre: "tarragona", comunidad: "catalunya"},
    {nombre: "alacant", comunidad: "valencia"},
    {nombre: "sevilla", comunidad: "andalucia"},
];

/* PASOS A SEGUIR:
(1) nueva carpeta vacía, la abrimos con visual code, abrimos terminal y escribimos:

npm init -y
npm install express ejs

(2) a continuación: 
creamos archivo .gitignore y escribimos la línea:
node_modules

(3) creamos las carpetas views y public

(4) editamos package.json y cambiamos scripts:
  "scripts": {
    "start": "nodemon index.js"
  },

(5) solo una vez en el primer proyecto express...

npm install -g nodemon

*/




app.use(express.static('public'))


app.get('/', function(req, res) { 
    let datos = {mostrar: false};
    res.render('index.ejs', datos);
});


app.get('/hola', function(req, res) { 
    const miComunidad = req.query.c;
    const ciudades = lista_ciudades.filter(c => c.comunidad===miComunidad);
    const datos = {nombre: 'Ana', apellido: "Roca", numero: 23, ciudades: ciudades}
    res.render('hola.ejs', datos);
});


app.listen(3000, function () {
    console.log('App a http://localhost:3000')
  });

