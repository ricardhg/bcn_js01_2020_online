import React from 'react';
import "./css/boton.css";


const CIRCULO = "fa fa-circle-o icono";
const CRUZ = "fa fa-times icono";

/*
El componente Boton recibe 3 props:
1) "orden" es la posición del botón en el tablero, de 1 a 9

2) "valor" es el valor del mapa de jugadas en la posición del botón. puede ser 0, 1 o 2
    si es 0 no se mostrará ningún icono
    si es 1 se muestra el círculo
    si es 2 se muestra la cruz

3) "jugada" es la función que debe ejecutarse cuando se clica en el botón
    a esta función se le envía como parámetro "orden"

Se muestran a continuación  3 versiones del componente, de más a menos "eficiente"
todas son correctas, pero la primera al tener menos líneas también tiene menos posibilidades de error y menos mantenimiento...

en los 3 casos se utilizan las constantes CIRCULO y CRUZ definidas al principio del módulo
*/

//versión "PRO"
export default (props) => 
        <div className="boton" onClick={()=>props.jugada(props.orden)}>
            <i className={props.valor===0 ? "" : props.valor===1 ? CIRCULO : CRUZ} aria-hidden="true"></i>  
        </div>;



//Versión con  function, reducida
/*
export default function Boton(props){
    let clasesIcono = "";
    if (props.valor>0){
        clasesIcono = (props.valor===1) ? CIRCULO : CRUZ;
    }
    return (
        <div className="boton" onClick={()=>props.jugada(props.orden)}>
             <i className={clasesIcono} aria-hidden="true"></i>  
        </div>
    );
}
*/


/* VERSION LARGA, CON CLASS
export default class Boton extends React.Component{

    constructor(props){
        super(props);

        this.clicar = this.clicar.bind(this);
    }

    clicar(){
        this.props.jugada(this.props.orden);
    }

    render(){

        let clasesIcono;

        if (valor>0){
            clasesIcono = (this.props.valor===1) ? CIRCULO : CRUZ;
        }

        return (
            <div className="boton" onClick={this.clicar}>
                 <i className={clasesIcono} aria-hidden="true"></i>  
            </div>

        );
    }
}
*/
