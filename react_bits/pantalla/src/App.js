import React from "react";
import { Container, Row, Col } from "reactstrap";

import Pantalla from './Pantalla.jsx';
import Boton from './Boton.jsx';


class App extends React.Component {


  constructor(props){
    super(props);

    this.state = {
      textoPantalla: "Selecciona"
    }

    this.cambia = this.cambia.bind(this);

  }

  cambia(nuevoTexto){
    this.setState({textoPantalla: nuevoTexto});
  }

  render(){
    return (
      <Container>
        <Row>
            <Col>
                <Pantalla texto={this.state.textoPantalla} />
            </Col>
        </Row>
        <Row>
          <Col>
            <Boton texto="Bici" clicar={this.cambia} />
          </Col>
          <Col>
            <Boton texto="Moto" clicar={this.cambia} />
          </Col>
          <Col>
            <Boton texto="Coche" clicar={this.cambia} />
          </Col>
        </Row>

      </Container>
    );
  }

}

export default App;