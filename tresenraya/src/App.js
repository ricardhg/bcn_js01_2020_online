import React from "react";
import { Container, Alert, Row, Col } from 'reactstrap';
import Boton from './Boton';


class App extends React.Component {

  constructor(props){

    super(props);
    this.state={
      turno: 1,
      mapa: [0,0,0, 0,0,0, 0,0,0],
      winner: 0,
      gameover: false
    }

    this.jugada = this.jugada.bind(this);
    this.analizaMapa = this.analizaMapa.bind(this);

  }

  

  jugada(botonClicado){

    //primero miramos si estamos en gameover o en winner
    //de ser así, el clic se interpreta como un reinicio de tablero
    //lo reiniciamos y salimos con return
    //para reiniciar ponemos el mapa a 0 y cambiamos también winner y gameover a sus valores iniciales
    if (this.state.winner>0 || this.state.gameover){
      this.setState({mapa: [0,0,0, 0,0,0, 0,0,0], winner: 0, gameover: false});
      return;
    }


    //miramos en el mapa qué hay en la posición del botón clicado
    let valorEnBotonClicado = this.state.mapa[botonClicado-1];

    //solo admitimos jugada si hay un 0 en la posición clicada
    if (valorEnBotonClicado===0){
      //jugada admitida!

      //creamos nuevo mapa cambiando al turno actual  en posición indicada
      let nuevoMapa = [...this.state.mapa];
      nuevoMapa[botonClicado-1]=this.state.turno;

      //cambiamos turno de 1 a 2 o de 2 a 1
      let nuevoTurno = (this.state.turno===1) ? 2: 1;

      //actualizamos mapa y turno en state
      //este cambio (el del mapa) se transmitirá a los botones automáticamente
      //a continuación llamamos a la función analizaMapa
      this.setState({mapa: nuevoMapa, turno: nuevoTurno}, this.analizaMapa );
    } else {
      //llegamos aquí si el usuario clica en un boton que ya está ocupado
      //de momento no hacemos nada pero podríamos mostrar un mensaje de error, reproducir un sonido...

    }
   
  }

  //se ejecuta después de cada jugada válida
  analizaMapa(){
    console.log("analizando jugadas...");
    //miramos si todavía quedan ceros en el mapa, de lo contrario "game over"
    //lo hacemos filtrando el mapa para obtener solo elemenos 0, y contando el array resultante
    let ceros = this.state.mapa.filter(el => el===0);
    if (ceros.length===0){
      this.setState({gameover: true});
    }    

    //aquí analizamos si hay una jugada ganadora, de forma muy simple solo miramos la primera línea
    // COMPLETAR O MEJORAR!!!
    // COMPLETAR O MEJORAR!!!
    // COMPLETAR O MEJORAR!!!
    // COMPLETAR O MEJORAR!!!
    // COMPLETAR O MEJORAR!!!
    // COMPLETAR O MEJORAR!!!
    // COMPLETAR O MEJORAR!!!
    // COMPLETAR O MEJORAR!!!
    // COMPLETAR O MEJORAR!!!
    if (this.state.mapa[0]>0 && this.state.mapa[0]===this.state.mapa[1] && this.state.mapa[1]===this.state.mapa[2]){
      let ganador = this.state.mapa[0];
      this.setState({winner: ganador});
    }
  }

  render() {


    let prueba = "";
    this.state.mapa.forEach(el => prueba=prueba+el)

    let mensajeGameOver = (this.state.gameover===true) ? <h1>Game Over</h1> : null;
    let mensajeWinner = (this.state.winner>0) ? <h1>{"Ganador: "+this.state.winner}</h1> : null;

 
    return (
      <Container>
        <h1>3 en raya</h1>
        <p>{prueba}</p>
        {mensajeGameOver}
        {mensajeWinner}
        <br />

        <Row>
          <Col>
            <Boton orden={1} valor={this.state.mapa[0]} jugada={this.jugada} />
          </Col>
          <Col>
            <Boton orden={2} valor={this.state.mapa[1]} jugada={this.jugada} />
          </Col>
          <Col>
            <Boton orden={3} valor={this.state.mapa[2]} jugada={this.jugada} />
          </Col>
        </Row>
        
        <Row>
          <Col>
            <Boton orden={4} valor={this.state.mapa[3]} jugada={this.jugada} />
          </Col>
          <Col>
            <Boton orden={5} valor={this.state.mapa[4]} jugada={this.jugada} />
          </Col>
          <Col>
            <Boton orden={6} valor={this.state.mapa[5]} jugada={this.jugada} />
          </Col>
        </Row>

        <Row>
          <Col>
            <Boton orden={7} valor={this.state.mapa[6]} jugada={this.jugada} />
          </Col>
          <Col>
            <Boton orden={8} valor={this.state.mapa[7]} jugada={this.jugada} />
          </Col>
          <Col>
            <Boton orden={9} valor={this.state.mapa[8]} jugada={this.jugada} />
          </Col>
        </Row>
      </Container>
    );
  }
}


export default App;

