
let CLAVE_LOCAL_STORAGE = "recetario_recetas";


class Receta {

 
    static guardar(datos){
        localStorage.setItem(CLAVE_LOCAL_STORAGE, JSON.stringify(datos));
    }

    static recuperar(){
        let datos = JSON.parse( localStorage.getItem(CLAVE_LOCAL_STORAGE) );
        if (datos===null){
            return [];
        }else{
            return datos;
        }
    }


    static getRecetas = () => {
        let recetas = this.recuperar();
        return recetas;
    } 

    static getRecetaById = (idReceta) => {
        let recetas = this.recuperar();
        let laReceta = recetas.find(el => el.id===idReceta*1);
        return laReceta;
    } 

    static addReceta = (nombreReceta, descripcionReceta) => {
        let recetas = this.recuperar();
        let max=0;
        recetas.forEach(el => {
            max = max<el.id ? el.id : max;
        })
        let nuevoId = max +1;
        let recetaNueva = {id: nuevoId, nombre: nombreReceta, 
            descripcion: descripcionReceta};
        recetas.push(recetaNueva);
        this.guardar(recetas);
    }

    static modificaReceta(idModificar, descripcionModificar){
        let recetas = this.recuperar();
        recetas = recetas.map(el => {
            if (el.id===idModificar*1){
                el.descripcion = descripcionModificar;
            }
            return el;
        });
        this.guardar(recetas);
    }


    static eliminaReceta = (idBorrar) => {
        let datos = this.recuperar();
        datos = datos.filter(el => el.id!==idBorrar*1);
        this.guardar(datos);
    }

}

export default Receta;