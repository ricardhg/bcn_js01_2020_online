
let CLAVE_LOCAL_STORAGE = "recetario_ingredientes";

class Ingrediente {


    static guardar(datos){
        localStorage.setItem(CLAVE_LOCAL_STORAGE, JSON.stringify(datos));
    }

    static recuperar(){
        let datos = JSON.parse( localStorage.getItem(CLAVE_LOCAL_STORAGE) );
        if (datos===null){
            return [];
        }else{
            return datos;
        }
    }

    static getIngredientes = (idRecetaPedida) => {
        let ingredientes = this.recuperar();
        return ingredientes.filter(el => el.idReceta === idRecetaPedida*1);
    } 

    static getTodosIngredientes = () => {
        let ingredientes = this.recuperar();
        return ingredientes;
    } 

    static addIngrediente = ({idReceta, idProducto, unidades, cantidad}) => {
        let max = 0;
        let ingredientes = this.recuperar();
        ingredientes.forEach(el => {
            max = (max < el.id) ? el.id : max;
        })
        let nuevoId = max + 1;
        let ingredienteNuevo = {
            id: nuevoId,
            idReceta: idReceta*1,
            idProducto: idProducto*1,
            cantidad: cantidad,
            unidades: unidades
        };

        ingredientes.push(ingredienteNuevo);
        this.guardar(ingredientes);
    }

    static eliminaIngrediente = (idBorrar) => {
        let datos = this.recuperar();
        datos = datos.filter(el => el.id !== idBorrar * 1);
        this.guardar(datos);
    }

}

export default Ingrediente;
