import React from 'react';

import "./css/capital.css";


//consultar: https://www.w3schools.com/js/js_string_methods.asp

class Capital extends React.Component {


    render(){

        //let inicial = this.props.nom[0].toUpperCase();
        let inicial = this.props.nom[0];
        inicial = inicial.toUpperCase();

        let ciudad = this.props.nom;
        ciudad = inicial + ciudad.substring(1).toLowerCase();
        let estilo = {
                color: this.props.color,
            };

        return (
            <div className="capital">
                <h1 style={estilo}>{inicial}</h1>
                <p>{ciudad}</p>
            </div>
        );
    }
}

export default Capital;