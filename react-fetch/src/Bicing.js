import React from 'react';
import { Button, Table, Container, Input, Row, Col } from 'reactstrap';

const API_URL = "https://api.citybik.es/v2/networks/bicing";


class Bicing extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            estaciones:[],
            minBicis:0,
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.carga = this.carga.bind(this);
    }
    
    handleInputChange(evento) {
        const target = evento.target;
        //const value = (target.type === 'checkbox') ? target.checked : target.value;
        const value = target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
      }

    extraeEstaciones(data){
        return data.network.stations;
    }

    //promise
    carga() {
        fetch(API_URL)
            .then(response => response.json())
            .then(data => data.network.stations)
            //.then(this.extraeEstaciones)
            .then(estaciones => this.setState({estaciones: estaciones}))
            .catch(error => console.log(error));
        
        console.log("Fetch iniciado");
     
    }

    render() {

        let estacionesFiltradas = this.state.estaciones
                    .filter(estacion => estacion.empty_slots>=this.state.minBicis)
                    .sort((a,b) => (a.empty_slots > b.empty_slots) ? -1 : 1);
        
        let filas =estacionesFiltradas.map((estacion, index) => (
            <tr key={index}>
                <td>{estacion.name}</td>
                <td>{estacion.longitude}</td>
                <td>{estacion.latitude}</td>
                <td>{estacion.empty_slots}</td>
            </tr>
        )

        );
        return (
            <>
                <Container>
                    <br />
                    <h2>Bicing</h2>
                    <br />
                    <Button onClick={this.carga} >Carga Bicis</Button>
                    <br />
                    <Row>
                        <Col >
                            <Input onChange={this.handleInputChange} type="range" min="0" max="50" name="minBicis" value={this.state.minBicis}/>
                        </Col>
                        <Col>
                            <h3>Capacidad mínima:{this.state.minBicis} 
                            Num estaciones: {estacionesFiltradas.length}</h3>
                        </Col>
                    </Row>

                    <br />
                    <Table>
                        <thead>
                            <tr>
                                <th>Dirección</th>
                                <th>Longitud</th>
                                <th>Latitud</th>
                                <th>Capacidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filas}
                        </tbody>
                    </Table>
                </Container>
            </>
        );
    }

}



export default Bicing;