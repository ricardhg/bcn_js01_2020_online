import React from 'react';
import Receta from './modelos/Receta';
import { Table, Button, Input, Col, Row } from 'reactstrap';
import {Link} from 'react-router-dom';


class ListaRecetas extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            recetas: Receta.getRecetas(),
            nombreReceta: "",
            descripcionReceta: ""
        }
        this.recetaNueva = this.recetaNueva.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.borra = this.borra.bind(this);
    }


    handleInputChange(evento) {
        const target = evento.target;
        //const value = (target.type === 'checkbox') ? target.checked : target.value;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }


    recetaNueva() {

        Receta.addReceta(this.state.nombreReceta,this.state.descripcionReceta);
        
        this.setState({ recetas: Receta.getRecetas(), 
            nombreReceta: "", descripcionReceta: "" })
    }

    borra(id) {
        Receta.eliminaReceta(id * 1);
        this.setState({ recetas: Receta.getRecetas() })
    }

    render() {
        // let recetas = Receta.getRecetaById(4);

        let filas = this.state.recetas.map(el =>
            <tr key={el.id}>
                <td>{el.id}</td>
                <td>{el.nombre}</td>
                <td>{el.descripcion}</td>
                <td>
                    <Button className="mr-2" color="danger" onClick={() => this.borra(el.id)}><i className="fa  fa-trash" aria-hidden="true"></i></Button>
                 
                    <Link to={"/EditaReceta/"+el.id} className="btn btn-success mr-2">
                        <i className="fa fa-edit" aria-hidden="true"></i>
                    </Link>
                  
                    <Link to={"/ModificaReceta/"+el.id} className="btn btn-info">
                        <i className="fa fa-edit" aria-hidden="true"></i>
                    </Link>
                </td>
            </tr>
        );

        return (
            <>
                <Row>
                    <Col>
                        <h2>Recetas</h2>
                        <br />
                    </Col>
                </Row>
                <Row>
                    <Col xs="4">
                        <h4>Nueva receta:</h4>
                        <Input value={this.state.nombreReceta} onChange={this.handleInputChange} type="text" name="nombreReceta" />
                        <br />
                        <Input value={this.state.descripcionReceta} onChange={this.handleInputChange} type="textarea" name="descripcionReceta" />
                        <br />
                        <Button disabled={this.state.nombreReceta.length < 2 || this.state.descripcionReceta.length<5} onClick={this.recetaNueva}>Añadir</Button>
                    </Col>
                    <Col >
                        <Table>
                            <tr>
                                <th>id</th>
                                <th>Receta</th>
                                <th>Descripción</th>
                                <th></th>
                            </tr>
                            {filas}
                        </Table>

                    </Col>


                </Row>

                <Row>

                </Row>

            </>

        )
    }

}

export default ListaRecetas;